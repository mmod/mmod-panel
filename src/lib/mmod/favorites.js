/*
 * SPDX-PackageName: MMOD Panel
 * SPDX-FileCopyrightText: ⓒ 2011 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0
 */


// DEFINES
const {
        Shell,
        St,
        Clutter,
        Meta
}                       = imports.gi;
const {
        main,
        appFavorites,
        appDisplay,
        dnd,
        dash
}                       = imports.ui;
const { config }        = imports.misc;


const SHELL_VER_3_3_OR_4 = ( config.PACKAGE_VERSION.startsWith( "3.3" ) || config.PACKAGE_VERSION.startsWith( "40" ) || config.PACKAGE_VERSION.startsWith( "41" ) || config.PACKAGE_VERSION.startsWith( "42" ) || config.PACKAGE_VERSION.startsWith( "43" ) );


class Modification {
    constructor( configuration ) {
        this._rigging = configuration.rigging;
        this._comfortSettings = configuration.comfortSettings;

        this._appSystem = null;
        this._panelBox = main.panel.actor.get_parent();
        this._container = main.panel._leftBox;

        this._box = null
        this._workId = null;

        this._connections = null;

        this._list = null;
        this._previous = null;
        this._favorites = null;
        this._running = null;
        this._displayed = null;

        this._dragCancelled = false;
        this._dragMonitor = null;

        this.loaded = null;
        this.active = false;
    }

    enable() {
        if( this._rigging.settings.get_boolean( 'display-favorites-enabled' ) ) {
            this._box = new St.BoxLayout( { name: 'favoritesBox', reactive: true, track_hover: true, clip_to_allocation: true } );

            // This asks Gnome to defer invocation of this method until it will not affect normal system operations?
            if( !this._workId )
                this._workId = main.initializeDeferredWork( this._box, this.update.bind( this ) );

            this._appSystem = Shell.AppSystem.get_default();
            this.active = true;

            // We must force an update regardless if deferred work is initialized
            this.update();
        }
    }

    disable() {

        if( this.active ) {
            // Disconnect any signals so we aren't causing problems
            this.disconnect();

            // Nullify oalist if necessary
            if( this._previous )
                this._previous = null;

            // Remove the list of favorite apps
            if( this._list )
                this._list = null;

            // Our list of favorite and running apps to display can be destroyed now
            if( this._displayed ) {
                for( let i in this._displayed ) {
                    // Remove the actor (array of favorites to be displayed) from the favorite container
                    //this.alaunch.remove_actor( this.inDisplay[o].item.actor );
                    this._displayed[i].item.actor.destroy();
                    this._displayed[i].item = null;
                }

                this._displayed = null;
            }

            // Remove the container used to display the array of favorite apps on the panel
            if( this.loaded ) {
                this._container.remove_child( this._box );
                this.loaded = false;
            }

            // Now delete our list of favorites
            if( this._favorites )
                this._favorites = null;

            // The list of running apps
            if( this._running )
                this._running = null;

            if( this._appSystem )
                this._appSystem = null;

            // The box
            if( this._box ) {
                this._box.destroy();
                this._box = null;
            }

            // And finally the workId since we'll need to get a new one
            if( this._workId )
                this._workId = null;

            this.active = false;
        }
    }

    /**
     * Method to update the display of favorites and/or running apps on the panel
     *
     * @params none
     *
     * @return void
     */
    update() {
        // Load the container to be displayed even prior to adding children to it in order to avoid get_theme_node() errors
        if( !this._loaded ) {
            let index = 1;
            if( !this._rigging.settings.get_boolean( 'favorites-before-preferences' ) )
                index = 2;

            this._container.insert_child_at_index( this._box, index );

            // Connect to the signals that are needed in order to properly reload the list of favorite/running apps at various times
            this.connect();

            this._loaded = true;
        }

        // First get the current map of favorites
        this._favorites = appFavorites.getAppFavorites().getFavoriteMap();

        // Next we create a list including apps which are supposed to be displayed on the panel based on current favorites list
        this._list = [];

        // Add the stored/registered favorites to the list
        for( let id in this._favorites ) {
            this._list.push( this._favorites[id] );
        }

        // Do the same for any running apps if applicable
        if( this._rigging.settings.get_boolean( 'show-running-apps' ) == true ) {
            this._running = this._appSystem.get_running();
            for( let i = 0; i < this._running.length; i++ ) {
                if( !( this._running[i].get_id() in this._favorites ) )
                    this._list.push( this._running[i] );
            }
        }

        // The obvious next step is to display an icon for each unique application within the display on the MMOD-Panel. However, we need to keep
        // in mind that as favorites are managed, apps executed, manipulated and closed, the icons for our displayed favorites and running apps
        // are required to respond to those changes. The icon's display each need to reflect the current global status.

        // This requires us to keep track of old and new positions so that we can properly update the existing display of favorites and/or running apps

        // But now we have a list of apps (favorite and/or running) in the old order they were in, and a list in the new order they are supposed
        // to be in. We also know the state and children of the current display.

        // If multiple instances of a single app are in the list, that application will feature an icon which
        // signifies it is running (hover state) constantly.  Clicking on that icon will display a list of all running instances to choose from.
        if( !this._displayed ) {
            this._displayed = [];
            this._previous = null;
        }
        else {
            this._previous = this._displayed;
            for( let i = 0; i < this._displayed.length; i++ ) {
                this._box.remove_actor( this._displayed[i].item.actor );
            }
            this._displayed = null;
            this._displayed = [];
        }

        let realIndex = 0;
        for( let item in this._list ) {
            // Let's us know whether to create a new app-well-icon or not, used below
            let skip = false;

            // Check if an old list exists - determines whether we execute some additional logic in hopes of improving efficiency
            if( this._previous ) {
                // See if the favorite at the current index has already been instantiated as an icon to be display previously
                if(  this._previous[item] && this._list[item] == this._previous[item].app ) {
                    // Great, the icon is already initialized and in display at the correct index, move it over
                    this._displayed.push( this._previous[item] );
                    this._displayed[realIndex].pos = realIndex;

                    // Let the method know to skip creating a new icon
                    skip = true;
                }
                else {
                    // It doesn't mean that its doesn't already exist, it could have been moved or repositioned as a result of a move
                    // let's do the dirty and search for it
                    for( let i in this._previous ) {
                        if( this._previous[i].app == this._list[item] ) {
                            // Move it over
                            this._displayed.push( this._previous[i] );
                            this._displayed[realIndex].pos = realIndex;

                            // Let the method know to skip creating a new icon
                            skip = true;
                        }
                    }

                }
            }

            if( !skip ) {
                // If we're not skipping, then push another object onto our in-display list
                this._displayed.push( { app: this._list[item], item: new appDisplay.AppIcon( this._list[item], {} ), pos: realIndex } );

                // Setup dnd handlers in the AppIcon container
                this._displayed[realIndex].item.handleDragOver = this.handleDragOver;
                this._displayed[realIndex].item.acceptDrop = this.acceptDrop;
                this._displayed[realIndex].item.settings = this._rigging.settings;

                // Immediately add the new actor the the app launch container
                this._box.add_actor( this._displayed[realIndex].item.actor );

                // Once the actor is on the stage let's start messing with its theme in order to avoid error
                let child = this._box.get_child_at_index( realIndex );

                // Set sizes based on settings, and attempt to support themes as best as possible
                let iconWidth = this._comfortSettings.favoriteSize, iconHeight = this._comfortSettings.favoriteSize;

                child.set_size( this._comfortSettings.containerSize, this._comfortSettings.containerSize );
                child.set_y_align( Clutter.ActorAlign.CENTER );

                /*
                    In gnome-shell ~v3.30+, the way favorites are rendered differs from the way they are rendered in
                    v3.16, which again differs from how they are rendered in v3.8 through 3.14. So here, we apply
                    some checks against how the icons are actualized to determine how to hack away at our favorites
                    icons in order to display them in our 'bar' properly
                */

                let has_newer_wrapping = false;
                let has_indicator = false;
                let has_newer_indicator = false;


                // 3.8 through 3.14 expressed the favorites icon in the most simplest of ways. The first child of the
                // widget seems to always be the icon structure:
                if( child.get_children()[0] &&
                    child.get_children()[0].has_style_class_name( 'overview-icon-with-label' ) ) {
                    // Remove the 'overview-icon-with-label' style to adjust the display (to that of just an overview-icon
                    // with no label):
                    child.get_children()[0].remove_style_class_name( 'overview-icon-with-label' );

                    // And reposition the icon structure so that its centered, as it was originally adjusted for a 'label' (?):
                    child.get_children()[0].set_position( 7, 10 );
                }
                else {
                    // 3.16+ introduced some new wrappings with its expression of each icon as a widget, its actor, and added
                    // an indicator to each icon structure:
                    has_newer_wrapping = true;

                    // The indicator's presence seems to be indicated (no pun intended :P) by the presence of more than one
                    // nested child (or child to the widget's immediate child) - !_typically_!

                    // As such, we !_typically_! found the icon structure itself as the first nested child (or first child to
                    // the first child) of the widget for the favorite when there was no indicator:
                    if( child.get_children()[0].get_children()[0] &&
                        child.get_children()[0].get_children()[0].has_style_class_name( 'overview-icon-with-label' ) ) {
                        // We remove the 'overview-icon-with-label' style to adjust the display (as we did previously):
                        child.get_children()[0].get_children()[0].remove_style_class_name( 'overview-icon-with-label' );

                        // Contrary to the 'norm' - 3.30+ has an indicator, but seems to stack the icon structure first.
                        //
                        // We can check that a second child exists by checking for more than one nested child (or child to
                        // the widget's child) within the widget structure:
                        if( ( child.get_children()[0].get_children() ).length > 1 ) {
                            // If there is more than 1 nested child (or child to the widget's child), there is an indicator;
                            // let's not reposition the icon (The indicator facility repositions all elements within the
                            // icon structure for us):
                            has_indicator = true;

                            // However, in shell versions prior to 3.30 the icon structure seems to have been stacked second.
                            // This required us to set the size of the !_second_! nested child's (or child to the
                            // widget's child's) first child to properly adjust the icon size - this will break our logic, and
                            // our display, to flag for this in version 3.30+ with out an adjustment to the logic; hence this
                            // secondary flag:
                            if( SHELL_VER_3_3_OR_4 )
                                has_newer_indicator = true;
                        }
                        else {
                            // If there is not more than 1 nested child (or child to the widget's child), we can
                            // reposition the icon so that it displays properly (with no indicator, removing the
                            // label requires manual adjustment):
                            child.get_children()[0].get_children()[0].set_position( 7, 10 );
                        }
                    }
                    else {
                        // From 3.16 through ~3.30, there is an indicator - however, it seems the icon structure itself
                        // stacked second (at least this check for a second nested child to the first child of the
                        // widget as the icon seems to imply as much):
                        if( child.get_children()[0].get_children()[1] &&
                            child.get_children()[0].get_children()[1].has_style_class_name( 'overview-icon-with-label' ) ) {
                            // So we'll need to remove the 'overview-icon-with-label' style from the second child
                            // to adjust the display:
                            child.get_children()[0].get_children()[1].remove_style_class_name( 'overview-icon-with-label' );

                            // But we won't reposition the icon if there's an indicator, as the indicator facility
                            // seems to reposition all elements within the widget's icon structure for us - based on
                            // the indicator's presence:
                            has_indicator = true;
                        }
                    }
                }

                // 3.8 through 3.14 continuation
                if( !has_newer_wrapping ) {
                    // The original wrapping requires that we set the icon size (according to prefs):
                    child.get_children()[0].get_children()[0].get_children()[0].set_size( iconWidth, iconHeight );

                    // And that we set the label (? or second child's) size to (0,0), for the display to work:
                    child.get_children()[0].get_children()[0].get_children()[1].set_size( 0, 0 );
                }
                else {
                    // 3.16 continuation
                    if( has_indicator && !has_newer_indicator ) {
                        // If there's another nested child (or child to the first child) to the widget stacked first
                        // we set the second nested child's (or second child to the first child) to the widget's first
                        // child's (the actual icon image) size (again, according to prefs):
                        child.get_children()[0].get_children()[1].get_children()[0].get_children()[0].set_size( iconWidth, iconHeight );

                        // And set the label's (? or second nested child to the widget's second child's) size to (0,0):
                        child.get_children()[0].get_children()[1].get_children()[0].get_children()[1].set_size( 0, 0 );
                    }
                    else {
                        // Otherwise, in the case that there is no indicator - or that the icon structure stacks first -
                        // we set the first nested child's (or first child to the first child of the
                        // widget's) first child's (the actual icon image) size (again, according to prefs):
                        child.get_children()[0].get_children()[0].get_children()[0].get_children()[0].set_size( iconWidth, iconHeight );

                        // And set the label (? or second child) size to (0,0):
                        child.get_children()[0].get_children()[0].get_children()[0].get_children()[1].set_size( 0, 0 );
                    }
                }
            }
            else
                this._box.add_actor( this._displayed[realIndex].item.actor );

            // Don't forget to increment
            realIndex++;
        }
    };

    /**
     * Method to requeue deferred work for the gnome-shell
     *
     * @params none
     *
     * @return void
     */
    queueUpdate() {
        if( this._workId )
            main.queueDeferredWork( this._workId );
    };

    /**
     * Method to initialize event handlers for signals related to the display of favorites on the panel
     *
     * @params none
     *
     * @return void
     */
    connect() {
        this._connections = [
            [
                this._appSystem,
                this._appSystem.connect(
                    'installed-changed',
                    () => {
                        appFavorites.getAppFavorites().reload();
                        this.queueUpdate();
                    }
                )
            ], [
                appFavorites.getAppFavorites(),
                appFavorites.getAppFavorites().connect( 'changed', this.queueUpdate.bind( this ) )
            ], [
                this._appSystem,
                this._appSystem.connect( 'app-state-changed', this.queueUpdate.bind( this ) )
            ], [
                main.overview,
                main.overview.connect( 'item-drag-begin', this.onDragBegin.bind( this ) )
            ], [
                main.overview,
                main.overview.connect( 'item-drag-end', this.onDragEnd.bind( this ) )
            ], [
                main.overview,
                main.overview.connect( 'item-drag-cancelled', this.onDragCancelled.bind( ) )
            ], [
                main.overview,
                main.overview.connect( 'showing', this.onToggleOverview.bind( this ) )
            ], [
                main.overview,
                main.overview.connect( 'hidden', this.onToggleOverview.bind( this ) )
            ]
        ];
    };

    /**
     * Method to remove the display of favorites from the panel
     *
     * @params none
     *
     * @return void
     */
    disconnect() {
        if( this._connections ) {
            for( let i = 0; i < this._connections.length; i++ ) {
                this._connections[i][0].disconnect( this._connections[i][1] );
            }
            this._connections = null;
        }
    };

    /**
     * Logic handler determining favorites visibility based on overview visibility
     *
     * @params none
     *
     * @return void
     */
    onToggleOverview() {
        if( this.loaded )
            if( main.overview.visible )
                this._box.hide();
            else
                this._box.show();
    };

    /**
     * Drag and Drop handler for item-drag-begin event
     *
     * @params none
     *
     * @return void
     */
    onDragBegin() {
        this._panelBox.raise( global.top_window_group );
        this._dragCancelled = false;
        this._dragMonitor = {
            dragMotion: this.onDragMotion.bind( this )
        };
        dnd.addDragMonitor( this._dragMonitor );
    };

    /**
     * Drag and Drop handler for item-drag-cancelled event
     *
     * @params none
     *
     * @return void
     */
    onDragCancelled() {
        this._dragCancelled = true;
        this.endDrag();
    };

    /**
     * Drag and Drop handler for item-drag-end event
     *
     * @params none
     *
     * @return none
     */
    onDragEnd() {
        if( this._dragCancelled )
            return;

        this.endDrag();
    };

    /**
     * Method to processes post-drag tasks
     *
     * @params none
     *
     * @return void
     */

    endDrag() {
        // Remove placeholder(s) from the alaunch
        let children = this._box.get_children();
        for( let i in children ) {
            if( children[i].has_style_class_name instanceof Function )
                if( children[i].has_style_class_name( 'dash-item-container' ) )
                    this._box.get_child_at_index( i ).destroy();
        }

        // Free up additional memory
        for( let child in this._displayed ) {
            if( child && child.item && child.item.placeholder ) {
                child.item.placeholder.destroy();
                child.item.placeholder = null;

                if( child.item.placeholderPos )
                    child.item.placeholderPos = null;
            }
        }

        dnd.removeDragMonitor( this._dragMonitor );

        this._panelBox.lower( main.messageTray.actor.get_parent() );
    };

    /**
     * Drag and Drop handler for DragMonitor events
     *
     * @param de    DragEvent   Defines params specific to the drag event
     *
     * @return dnd.DragMotionResult
     */

    onDragMotion( de ) {
        let app = dash.getAppFromSource( de.source );
        if( !app )
            return dnd.DragMotionResult.CONTINUE;

        return dnd.DragMotionResult.CONTINUE;
    };

    /**
     * Drag and Drop handler for DragMonitor events
     *
     * @param s     Defines the source object
     * @param a     Defines the dragged actor
     * @param x     Defines the x position of the mouse(source) within the target
     * @param y     Defines the y position of the mouse(source) within the target
     * @param t     Defines the time of the event
     *
     * @return dnd.DragMotionResult
     */
    handleDragOver( s, a, x, y, t ) {
        if( !this._rigging.settings.get_boolean( 'display-favorites-enabled' ) )
            return dnd.DragMotionResult.NO_DROP;

        let app = dash.getAppFromSource( s );
        if( !app || app.is_window_backed() )
            return dnd.DragMotionResult.NO_DROP;

        let favorites = appFavorites.getAppFavorites().getFavorites();

        let cafavPos = favorites.indexOf( app );
        let ctfavPos = favorites.indexOf( this.app );

        // If the dragged actor (cafavPos) is not in the list of favorites (-1), the move is not allowed
        // If the target actor (ctfavPos) is not in the list of favorites (-1 ), the move is not allowed
        if( ctfavPos == -1 )
            return dnd.DragMotionResult.NO_DROP;

        let move = false;
        if( cafavPos > -1 )
            move = true;


        // Otherwise the dragged item can be moved to the target position, let's put a placeholder there which
        // will display until the drag is ended or the dragged actor is moved to another position.

        // Since this could be that extended drag to a new position, let's test to see if we have any placeholders
        // in the alaunch that need to be removed:
        let children = a.get_parent().get_children();                                   // The parent is UIGroup
        let panelBoxIndex = children.indexOf( main.panel.actor.get_parent() );          // Get the index of the panelBox
        let panelBox = a.get_parent().get_child_at_index( panelBoxIndex );              // To get a handle to the panelBox

        // Next we figure out which index the favoritesBox should be within the leftBox of the panel
        let favoritesBoxIndex = 1;
        if( !this._rigging.settings.get_boolean( 'favorites-before-preferences' ) )
            favoritesBoxIndex = 2;

        // And get a handle to our favorites box. We had to go through all this trouble because handleDragOver belongs to the
        // app-well-icon, and not to our favorites modification; making this.box and this.container unavailable to us.
        let favoritesBox = panelBox.get_child_at_index( 0 ).get_child_at_index( 0 ).get_child_at_index( favoritesBoxIndex );

        // Now go ahead and remove all found placeholders
        for( let i = 0; i < favoritesBox.get_n_children(); i++ ) {
            let child = favoritesBox.get_child_at_index( i );
            if( child.has_style_class_name instanceof Function )
                if( child.has_style_class_name( 'dash-item-container' ) )
                    child.destroy();
        }

        // And create a new placeholder (moving just leads to need to iterate again anyhow to remove any extras)
        this.placeholder = null;
        this.placeholderPos = null;

        // This is a check to ensure we're actually making a move, and also
        // allows us to configure where the placeholder will go so the item
        // is moved to the appropriate 'side' of the target actor.
        if( cafavPos < ctfavPos )
            this.placeholderPos = ctfavPos + 1;
        else // We can't let == work.
            if( cafavPos > ctfavPos )
                this.placeholderPos = ctfavPos;
            else
                return dnd.DragMotionResult.NO_DROP;

        // If we're going to display a placeholder
        if( this.placeholderPos !== null ) {
            this.placeholder = new dash.DragPlaceholderItem();
            this.placeholder.child.set_width( 12 );
            this.placeholder.child.set_height( 36 );

            // Insert it at the target position
            favoritesBox.insert_child_at_index( this.placeholder, this.placeholderPos );

            // Then show it once it's on the stage to avoid errors
            this.placeholder.show( true );
        }

        if( move )
            return dnd.DragMotionResult.MOVE_DROP;

        return dnd.DragMotionResult.COPY_DROP;
    };

    /**
     * Drag and Drop handler for DragMonitor events
     *
     * @param s     Defines the source object
     * @param a     Defines the dragged actor
     * @param x     Defines the x position of the mouse(source) within the target
     * @param y     Defines the y position of the mouse(source) within the target
     * @param t     Defines the time of the event
     *
     * @return dnd.DragMotionResult | boolean
     */
    acceptDrop( s, a, x, y, t ) {
        if( !this._rigging.settings.get_boolean( 'display-favorites-enabled' ) )
            return true;

        let app = dash.getAppFromSource( s );
        if( !app || app.is_window_backed() )
            return false;

        let favorites = appFavorites.getAppFavorites().getFavorites();

        let cafavPos = favorites.indexOf( app );
        let ctfavPos = favorites.indexOf( this.app );

        let move = false;
        // If the target actor (ctfavPos) is not in the list of favorites (-1 ), the move or addition is not handled by steward
        if( ctfavPos == -1 )
            return false;

        // As long as the dragged actor (cafavPos) is in the list of favorites (> -1), its a move (if we've gotten this far, anyhow)
        if( cafavPos > -1 )
            move = true;

        // Let's see if the user meant to make a move or addition, but first we'll return true if it is being dropped someplace without
        // a placeholder since that would mean we put it back to its original position
        if( !this.placeholder )
            return true;

        // Let's get the id of the application
        let id = app.get_id();

        // And actually manipulate our favorites list accordingly
        Meta.later_add(
            Meta.LaterType.BEFORE_REDRAW,
            () => {
                let favorites = appFavorites.getAppFavorites();
                if( move )
                    favorites.moveFavoriteToPos( id, ctfavPos );
                else
                    favorites.addFavoriteAtPos( id, ctfavPos );
            }
        );

        return false;
    };
}
