/*
 * SPDX-PackageName: MMOD Panel
 * SPDX-FileCopyrightText: ⓒ 2011 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0
 */


// DEFINES
const {
        config
}                                   = imports.misc;
const   GNOME_SHELL_VERSION_PARTS   = config.PACKAGE_VERSION.split( '.' ),
        SHELL_VERSION_MAJOR         = Number.parseInt( GNOME_SHELL_VERSION_PARTS[0] ),
        SHELL_VERSION_MINOR         = Number.parseInt( GNOME_SHELL_VERSION_PARTS[1] ),
        USE_GTK4                    = !( SHELL_VERSION_MAJOR < 40 ),
        USE_CONVENIENCE             = !USE_GTK4 && SHELL_VERSION_MINOR < 36,
        USE_ESYS                    = !USE_GTK4 && SHELL_VERSION_MINOR <= 2;
const {
        Shell,
        St,
        Meta
}                                   = imports.gi;
const {
        main,
        layout
}                                   = imports.ui;
const {
        mainloop,
        tweener
}                                   = imports;

const   HOT_CORNER_DISABLED         = 54321;
const   PRESSURE_TIMEOUT            = 1000;


class Modification {
    constructor( configuration ) {
        this._rigging = configuration.rigging;

        this._autohideBehavior = new EnableAutoHideBehavior( configuration );
        this._hotCornerBehavior = new DisableHotCornerBehavior( configuration );

        this.active = false;
    }

    enable() {
        // We invoke init regardless because of certain processes that need
        // to happen for supporting real-time application of preference changes
        this._autohideBehavior.enable();

        this._hotCornerBehavior.enable();

        this.active = true;
    }

    disable() {
        if( this.active ) {
            // And we always call disable so that realize is properly handled.
            this._autohideBehavior.disable();

            // hotCorners is always active anyhow as well.
            this._hotCornerBehavior.disable();

            this.active = false;
        }
    }
}


class EnableAutoHideBehavior {
    constructor( configuration ) {
        this._rigging           = configuration.rigging;

        // Check for barrier support
        this._barriers          = global.display.supports_extended_barriers();
        this._pressureBarrier   = null;
        this._pressureThreshold = null;
        this._barrier           = null;
        this._barrierTriggered  = null;

        this._panelBox          = null;
        this._uiGroup           = null;

        this._monitor           = main.layoutManager.primaryMonitor;

        this._hoverBox          = null;
        this._hoverStates       = null;
        this._visibleStates     = null;
        this._hidden            = null;
        this._config            = null;

        this._connections       = null;

        this._favorites         = null;
        this._activities        = main.panel.statusArea.activities;
        this._app               = main.panel.statusArea.appMenu;
        this._date              = main.panel.statusArea.dateMenu;
        this._aggregate         = main.panel.statusArea.aggregateMenu;

        this._realize           = { panelBox: null, uiGroup: null };

        this.active             = false;
    }

    enable() {
        if( this._rigging.settings.get_boolean( 'autohide-panel' ) ) {
            this._panelBox          = main.panel.actor.get_parent();
            this._uiGroup           = this._panelBox.get_parent();

            // Handle realize if applicable
            if( ( !this._panelBox && !this._realize.panelBox ) || ( !this._uiGroup && !this._realize.uiGroup ) )  {
                // Shell/Extension has just initialized
                if( !this._panelBox )
                    this._realize.panelBox = this._panelBox.connect( 'realize', this.enable.bind( this ) );

                if( !this._uiGroup )
                    this._realize.uiGroup = this._uiGroup.connect( 'realize', this.enable.bind( this ) );

                // Do not allow the method to continue
                return;
            }

            if( this._panelBox && this._uiGroup && this._realize.panelBox && this._realize.uiGroup ) {
                // We're initializing after a disable/enable combo-invocation
                this._panelBox.disconnect( this._realize.panelBox );
                this._uiGroup.disconnect( this._realize.uiGroup );

                this._realize = { panelBox: false, uiGroup: false };
            }

            if( this._barriers ) {
                // Get a handle to the appSystem for later use
                this._appSystem = shell.AppSystem.get_default();

                // Get the pressure threshold setting
                this._pressureThreshold = this._rigging.settings.get_double( 'autohide-pressure-threshold' );

                // Create a new pressure barrier for triggering the panel into display when hidden
                this._pressureBarrier = new layout.PressureBarrier(
                    this._pressureThreshold,
                    PRESSURE_TIMEOUT,
                    shell.KeyBindingMode.NORMAL | shell.KeyBindingMode.OVERVIEW
                );

                // Connect to the trigger signal that will be emitted by our pressure barrier so we can handle it.
                this._pressureBarrier.connect(
                    'trigger',
                    barrier => this.onBarrierTriggered()
                );

                // We need to know the edge the panel is on so that we can create - and properly position  - a box for
                // detecting mouse moves off of the panel.
                let x = 0, y;
                switch( main.panel.edge ) {
                    case 0:
                        // If on the bottom, we want the hover detection device to sit primarily above the panel with a 5 pixel clip.
                        // The clip fixes a notify::hover signal emit issue.
                        y = this._monitor.height - ( this._panelBox.get_height() * 2 ) + 5;
                        break;

                    case 1:
                        // If on top we still want the clip, but we want the hover detection device to sit primarily below the panel
                        y = this._monitor.y + ( this._panelBox.get_height() - 5 );
                        break;
                }

                // Let's create that hover detection device we've been going on about
                this._hoverBox = new St.BoxLayout( { name: 'autohideHoverBox', reactive: true, track_hover: true } );
                this._hoverBox.triggered = false;

                // Then add it to the uiGroup so that we - not only have it on the stage to be manipulated, but - can toy with its z-index
                this._uiGroup.add_actor( this._hoverBox );

                // Now that its on the stage let's toy with its appearance and position.
                this._hoverBox.set_height( this._panelBox.get_height() );
                this._hoverBox.set_width( this._monitor.width );
                this._hoverBox.set_position( x, y );
                this._hoverBox.set_x = x;
                this._hoverBox.set_y = y;

                // And then its z-index. It needs to be above the panelBox - and clipping it by at least 1 pixel - to trigger events properly.
                this._hoverBox.raise( this._panelBox );

                // We initialize a flag for hover states of the panel's actors (built-in menus)
                this._hoverStates = {
                    activities: false,  // panelButton
                    favorites: false,   // favorites
                    app: false,         // appMenu
                    date: false,        // Date/Time/System menu
                    aggregate: false,   // Aggregate menu
                    hoverBox: false     // the hoverbox
                };

                // And a flag for visible states of the panel's actors (built-in menus which have a true pop-up; i.e. excludes activities)
                this._visibleStates = {
                    activities: false,  // panelButton
                    favorites: false,   // favorites
                    app: false,         // appMenu
                    date: false,        // Date/Time/System menu
                    aggregate: false    // Aggregate menu
                };

                // Prepare settings for our event handlers
                this._configuration = {
                    delay: ( this._rigging.settings.get_double( 'autohide-delay' ) * 1000 ),
                    animationTime: this._rigging.settings.get_double( 'autohide-animation-time' ),
                    animationDelay: this._rigging.settings.get_double( 'autohide-animation-delay' )
                };

                // If favorites are displayed, we will fetch the actor and make sure to connect our event handlers to it
                // so we can properly process autohide logic for the panel.
                if( this._rigging.settings.get_boolean( 'display-favorites-enabled' ) ) {
                    let index = 1;
                    if( !this._rigging.settings.get_boolean( 'favorites-before-preferences' ) )
                        index = 2;

                    this._favorites = main.panel._leftBox.get_child_at_index( index );
                }

                // Setup handling of hover notifications for autohide logic
                this.connect();

                // Initially hide the panel
                this.hide();

                this.active = true;
            }
        }
    }

    disable() {
        if( this.active ) {
            // Disconnect any signals so we aren't causing problems
            this.disconnect();

            // Remove the autohide barrier
            this.removeBarrier();

            // The pressure barrier
            if( this._pressureBarrier ) {
                this._pressureBarrier.destroy();
                this._pressureBarrier = null;
                this._pressureThreshold = null;
                this._barrierTriggered = null;
            }

            // The autohide Hover box
            if( this._hoverBox ) {
                this._uiGroup.remove_actor( this._hoverBox );
                this._hoverBox.destroy();
                this._hoverBox = null;
            }

            // And all our variables/flags
            if( this._hoverStates )
                this._hoverStates = null;

            if( this._visibleStates )
                this._visibleStates = null;

            if( this._configuration )
                this._configuration = null;

            if( this._favorites )
                this._favorites = null;

            this.active = false;
        }
    }

    /**
     * Method to hide the panel
     *
     * @params none
     *
     * @return void
     */
    hide() {
        // Here we tween the panel up or down off of the viewable stage depending on panel position
        let y;
        switch( main.panel.edge ) {
            case 0:
                y = this._monitor.height;
                break;

            case 1:
                y = this._monitor.y - this._panelBox.get_height();
                break;
        }

        tweener.addTween(
            this._panelBox,
            {
                y: y,
                time: this._configuration.animationTime,
                delay: this._configuration.animationDelay,
                transition: 'easeOutQuad',
                onComplete: () => {
                    // Then instantiate a barrier for our pressure barrier for toggling the panel into display once hidden
                    this.addBarrier();

                    // Set the autohide status
                    this._hidden = true;

                    // Hide the autohideHoverBox while the panel is hidden
                    this._hoverBox.hide();
                }
            }
        );
    };

    /**
     * Method to show the panel
     *
     * @params none
     *
     * @return void
     */
    show() {
        // Here we tween the panel up or down on to the viewable stage depending on panel position
        let y;
        switch( main.panel.edge ) {
            case 0:
                y = this._monitor.height - this._panelBox.get_height();
                break;

            case 1:
                y = this._monitor.y;
                break;
        }

        tweener.addTween(
            this._panelBox,
            {
                y: y,
                time: this._configuration.animationTime,
                delay: this._configuration.animationDelay,
                transition: 'easeOutQuad',
                onComplete: () => {
                    // Remove the barrier for our pressure barrier to avoid
                    // issues with accessing the viewable stage while the panel
                    // is shown
                    this.removeBarrier();

                    // Set the autohide status
                    this._hidden = false;

                    // Set autohideHoverBoxTriggered flag and unhide the
                    // autohideHoverBox so it can be triggered
                    this._hoverBox.triggered = false;

                    if( !main.overview.visible )
                        this._hoverBox.show();

                    // Set a delayed invocation of hoverChanged to start
                    // autohide logic, It'll flow out so no need to trace the
                    // mainloop timeout
                    mainloop.timeout_add(
                        this._configuration.delay,
                        () => this.hoverChanged()
                    );
                }
            }
        );
    }

    /**
     * Method to start auto-hide logic when the pressure barrier is triggered
     *
     * @params none
     *
     * @return void
     */
    onBarrierTriggered() {
        this._barrierTriggered = true;

        this.hoverChanged();
    }

    /**
     * Method for processing auto-hide logic
     *
     * @params none
     *
     * @return void
     */
    hoverChanged() {
        // Here we check if any of the visible elements of the panel are hovered.
        // If so we set up a 3 second delayed invocation of this method and
        // check again. If none of the elements are hovered we hide the panel,
        // unless barrierTriggered is set to true, in which case we set
        // barrierTriggered to false and set up a 3 second delayed invocation of
        //  this method to check again like above. We also check if the hoverBox
        // has been tripped while nothing is hovered, and if so we go ahead and
        // hide the panel - even if barrierTriggered is true since the user
        // would have triggered that flag.
        if( !this._hoverStates )
            return;

        let hovered = false, hiding = this._hoverStates[5];
        for( let i in this._hoverStates ) {
            if( this._hoverStates[i] && i !== 'hoverBox' )
                hovered = true;
        }

        for( let i in this._visibleStates ) {
            if( this._visibleStates[i] )
                hovered = true;
        }

        if( hiding && !hovered ) {
            if( !this._hidden )
                this.hide();

            return;
        }

        if( hovered || this._barrierTriggered || main.overview.visible ) {
            // Reset barrierTriggered flag
            this._barrierTriggered = false;

            // Delay an invocation of this method to check conditions again after ensuring panel is shown
            if( this._hidden )
                this.show();
            else {
                // Set autohideHoverBoxTriggered flag and unhide the autohideHoverBox so it can be triggered
                this._hoverBox.triggered = false;
                this._hoverBox.show();

                // Our logic does not loop infinitly - we do not need to trace this mainloop timeout:
                mainloop.timeout_add(
                    this._configuration.delay,
                    () => this.hoverChanged()
                );
            }

            return;
        }

        // If for some reason this method was invoked but none of the above cases are true, check whether
        // the panel is hidden, and if not let us hide it.
        if( !this._hiden && !hovered )
            this.hide();
    }

    /**
     * Method to set event handlers used in auto-hide logic
     *
     * @params none
     *
     * @return void
     */
    connect() {

        this._connections = [
            [
                this._hoverBox,
                this._hoverBox.connect( 'notify::hover', this.boxHoverChanged.bind( this ) )
            ], [
                this._activities.actor,
                this._activities.actor.connect( 'notify::hover', this.activitiesHoverChanged.bind( this ) )
            ], [
                this._date.actor,
                this._date.actor.connect( 'notify::hover', this.dateHoverChanged.bind( this ) )
            ], [
                this._date.menu,
                this._date.menu.connect( 'open-state-changed', this.dateStateChanged.bind( this ) )
            ], [
                this._aggregate.actor,
                this._aggregate.actor.connect( 'notify::hover', this.aggregateHoverChanged.bind( this ) )
            ], [
                this._aggregate.menu,
                this._aggregate.menu.connect( 'open-state-changed', this.aggregateStateChanged.bind( this ) )
            ], [
                this._app.actor,
                this._app.actor.connect( 'notify::hover', this.appPrefsHoverChanged.bind( this ) )
            ], [
                this._appSystem,
                this._appSystem.connect( 'app-state-changed', this.appStateChanged.bind( this ) )
            ], [
                main.overview,
                main.overview.connect( 'showing', this.onToggleOverview.bind( this ) )
            ], [
                main.overview,
                main.overview.connect( 'hidden', this.onToggleOverview.bind( this ) )
            ]
        ];

        if( this._favorites )
            this._connections.push(
                [
                    this._favorites,
                    this._favorites.connect( 'notify::hover', this.favoritesHoverChanged.bind( this ) )
                ]
            );
    }

    /**
     * Method to remove event handlers used in auto-hide logic
     *
     * @params none
     *
     * @return void
     */
    disconnect() {
        if( this._connections ) {
            for( let i = 0; i < this._connections.length; i++ ) {
                this._connections[i][0].disconnect( this._connections[i][1] );
            }

            this._connections = null;
        }
    }

    /**
     * Method to initialize the barrier for triggering the panel when hidden and auto-hide is enabled
     *
     * @params none
     *
     * @return void
     */
    addBarrier() {
        let x = 0, y;
        switch( main.panel.edge ) {
            case 0:
                y = this._monitor.height;
                break;

            case 1:
                y = this._monitor.y;
                break;
        }

        let direction;
        if( this._panelBox.get_text_direction() === 2 )
            direction = meta.BarrierDirection.NEGATIVE_X;
        else
            direction = meta.BarrierDirection.POSITIVE_X;

        // Create an actual barrier to be triggered by pressure sensing (will block status panel on bottom)
        this._barrier = new meta.Barrier(
            {
                display: global.display,
                x1: x,
                x2: this._monitor.width,
                y1: y,
                y2: y,
                directions: direction
            }
        );

        if( this._pressureBarrier )
            this._pressureBarrier.addBarrier( this._barrier );
    }

    /**
     * Method to disable the barrier for triggering the panel when it is hidden and auto-hide is enabled
     *
     * @params none
     *
     * @return void
     */
    removeBarrier() {
        if( this._barrier ) {
            if( this._pressureBarrier ) {
                this._pressureBarrier.removeBarrier( this._barrier );

                // We are required to manually reset this flag since we remove the barrier the avoid problems
                // when we show the panel
                this._pressureBarrier._isTriggered = false;
            }

            this._barrier.destroy();
            this._barrier = null;
        }
    }

    /**
     * Event handler for auto-hide logic processing
     *
     * @params none
     *
     * @return void
     */
    boxHoverChanged() {
        if( this._hoverStates ) {
            if( this._hoverStates.hoverBox )
                this._hoverStates.hoverBox = false;
            else
                this._hoverStates.hoverBox = true;

            this._hoverBox.triggered = true;
        }
    }

    /**
     * Event handler for auto-hide logic processing
     *
     * @params none
     *
     * @return void
     */
    favoritesHoverChanged() {
        if( this._hoverStates )
            if( this._hoverStates.favorites )
                this._hoverStates.favorites = false;
            else
                this._hoverStates.favorites = true;
    }

    /**
     * Event handler for auto-hide logic processing
     *
     * @params none
     *
     * @return void
     */
    activitiesHoverChanged() {
        if( this._hoverStates )
            if( this._hoverStates.activities )
                this._hoverStates.activities = false;
            else
                this._hoverStates.activities = true;
    }

    /**
     * Event handler for auto-hide logic processing
     *
     * @params none
     *
     * @return void
     */
    activitiesStateChanged() {
        if( this._visibleStates )
            if( this._visibleStates.activities )
                this._visibleStates.activities = false;
            else
                this._visibleStates.activities = true;
    }

    /**
     * Event handler for auto-hide logic processing
     *
     * @params none
     *
     * @return void
     */
    dateHoverChanged() {
        if( this._hoverStates )
            if( this._hoverStates.date )
                this._hoverStates.date = false;
            else
                this._hoverStates.date = true;
    }

    /**
     * Event handler for auto-hide logic processing
     *
     * @params none
     *
     * @return void
     */
    dateStateChanged() {
        if( this._visibleStates )
            if( this._visibleStates.date )
                this._visibleStates.date = false;
            else
                this._visibleStates.date = true;
    }

    /**
     * Event handler for auto-hide logic processing
     *
     * @params none
     *
     * @return void
     */
    aggregateHoverChanged() {
        if( this._hoverStates )
            if( this._hoverStates.aggregate )
                this._hoverStates.aggregate = false;
            else
                this._hoverStates.aggregate = true;
    }

    /**
     * Event handler for auto-hide logic processing
     *
     * @params none
     *
     * @return void
     */
    aggregateStateChanged() {
        if( this._visibleStates )
            if( this._visibleStates.aggregate )
                this._visibleStates.aggregate = false;
            else
                this._visibleStates.aggregate = true;
    }

    /**
     * Event handler for auto-hide logic processing
     *
     * @params none
     *
     * @return void
     */
    appPrefsHoverChanged() {
        if( this._hoverStates )
            if( this._hoverStates.app )
                this._hoverStates.app = false;
            else
                this._hoverStates.app = true;
    }

    /**
     * Event handler for auto-hide logic processing
     *
     * @params none
     *
     * @return void
     */
    appStateChanged() {
        if( this._rigging.settings.get_boolean( 'autohide-panel' ) )
            this.hide();
    }

    /**
     * Event handler for auto-hide logic processing
     *
     * @params none
     *
     * @return void
     */
    onToggleOverview() {
        if( this._rigging.settings.get_boolean( 'autohide-panel' ) ) {
            if( main.overview.visible ) {
                this.show();

                let x = 0, y;
                switch( main.panel.edge ) {
                    case 0:
                        y = this._monitor.height;
                        break;

                    case 1:
                        y = this._monitor.y - this._panelBox.get_height();
                        break;
                }

                this._hoverBox.set_position( x, y );
            }
            else {
                this.hide();

                let x = 0, y;
                switch( main.panel.edge ) {
                    case 0:
                        y = this._monitor.height - ( this._panelBox.get_height() * 2 ) + 5;
                        break;

                    case 1:
                        y = this._monitor.y + ( this._panelBox.get_height() - 5 );
                        break;
                }

                this._hoverBox.set_position( x, y );
            }
        }
    }
}


class DisableHotCornerBehavior {
    constructor( configuration ) {
        this._rigging       = configuration.rigging;

        this._panelBox      = null;

        // Checks for barrier support
        this._barriers       = global.display.supports_extended_barriers();

        // Store original threshold and toggleOverview values
        this._threshold      = main.layoutManager.hotCorners[main.layoutManager.primaryIndex]._pressureBarrier._threshold;
        this._toggleOverview = main.layoutManager.hotCorners[main.layoutManager.primaryIndex]._toggleOverview;


        this._realize       = false;
        this.active         = false;
    }

    enable() {
        // Check if hot corners have been disabled:
        if( this._rigging.settings.get_boolean( 'hot-corner-disabled' ) ) {
            this._panelBox = main.panel.actor.get_parent()

            // Handle realize if applicable
            if( !this._panelBox && !this._realize ) {
                // Shell/Extension has just initialized
                this._realize = this._panelBox.connect( 'realize', this.init.bind( this ) );

                // Do not allow the method to continue
                return;
            }

            if( this._panelBox && this._realize ) {
                // We're initializing after a disable/enable combo-invocation
                this._panelBox.disconnect( this._realize );
                this._realize = false;
            }

            if( this._barriers ) {
                main.layoutManager.hotCorners[main.layoutManager.primaryIndex]._pressureBarrier._threshold = HOT_CORNER_DISABLED;
                main.layoutManager.hotCorners[main.layoutManager.primaryIndex]._toggleOverview = HOT_CORNER_DISABLED;

                this.active = true;
            }
        }
    }

    disable() {
        if( this._barriers ) {
            main.layoutManager.hotCorners[main.layoutManager.primaryIndex]._pressureBarrier._threshold = this._threshold;
            main.layoutManager.hotCorners[main.layoutManager.primaryIndex]._toggleOverview = this._toggleOverview;

            this.active = false;
        }
    }
}
